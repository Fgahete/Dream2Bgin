<div class="rte">
    <h1>Nos actus</h1>
    {foreach from=$affiche_actus item=uneactu}
        <h2>{$uneactu.titre}</h2>
        <p>{$uneactu.actualite} </p>
        <p class="pfoot">Actualité n°{$uneactu.id_modactua} / publiée le : {$uneactu.date_add}</p>
    {/foreach}
</div>