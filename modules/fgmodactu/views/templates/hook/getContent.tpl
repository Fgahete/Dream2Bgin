{if isset($confirmation)}
    <div class="alert alert-success">La configuration a bien été mise à jour</div>
{/if}
<form method="post" action="" class="defaultForm form-horizontal">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i> Configuration du module
        </div>
        <div class="form-wrapper">
            <div class="form-group">
                <label for="idactu">N° de l'actualité:</label>
                <input type='text' name="idactu" id="idactu"
                          class="form-control">
            </div>
        <div class="form-wrapper">
            <div class="form-group">
                <label for="titre">Titre:</label>
                <input type='text' name="titre" id="titre"
                          class="form-control">
            </div>
            <div class="form-group">
                <label for="actualite">Actualite:</label>
                <textarea name="actualite" id="actualite"
                          class="form-control"></textarea>
            </div>
                        <div class="form-group">
                <label for="date_add">Date : </label>
                <textarea name="date_add" id="date_add"
                          class="form-control"></textarea>
            </div>
        </div>
        <div class="panel-footer">
            <button class="btn btn-default pull-right" name="submit_fgmodactu_form" value="1" type="submit">
                <i class="process-icon-save"></i> Enregistrer
            </button>
        </div>
    </div>
</form>