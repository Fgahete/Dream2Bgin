<?php

class fgmodactu extends Module {

    //construction du module
    public function __construct() {
        $this->name = 'fgmodactu';
        $this->tab = 'front_office_features';
        $this->version = '0.1.0';
        $this->author = 'Catchu';

        $this->displayName = 'Module pour Actualités';
        $this->description = 'Un module pour afficher une liste d\'actualités';
        $this->bootstrap = true;
        parent::__construct();
    }

    public function install() {
        parent::install();
        $this->registerHook('displayHome');
        return true;
    }
    
   /* public function uninstall() {
        if (!parent::uninstall()) {
            return false;
        }
        Configuration::deleteByName('FGMOD_IDACTU', '');
        Configuration::deleteByName('FGMOD_TITRE', '');
        Configuration::deleteByName('FGMOD_ACTUALITE', '');

        return true;
    }*/

    //traitement du soumission du formulaire back
    public function processConfiguration() {
        if (Tools::isSubmit('submit_fgmodactu_form')) {//permet de savoir si le formulaire a bien été envoyé
            $id_actu = Tools::getValue('id_actu'); //sauvegarde les données du formulaire
            $titre = Tools::getValue('titre');
            $actualite = Tools::getValue('actualite');

            $insert = array(//après création de la table ds la BdD de Prestashop, ceci permet de rentrer l'actu dans celle-ci
                'id_actu' => (int) $id_actu,
                'titre' => pSQL($titre),
                'actualite' => pSQL($actualite),
                'date_add' => date('Y-m-d H:i:s'),
            );
            Db::getInstance()->insert('modactua', $insert);
            Configuration::updateValue('FGMOD_IDACTU', $id_actu);
            Configuration::updateValue('FGMOD_TITRE', $titre);
            Configuration::updateValue('FGMOD_ACTUALITE', $actualite);
            Configuration::updateValue('FGMOD_DATEADD', $date_add);
            $this->context->smarty->assign('confirmation', 'ok'); //affiche un message de confirmation
        }
    }

    //permet de récupérer les valeurs de configuration
    public function assignConfiguration() {
        $id_actu = Configuration::get('FGMOD_IDACTU');
        $titre = Configuration::get('FGMOD_TITRE');
        $actualite = Configuration::get('FGMOD_ACTUALITE');
        $date_add = Configuration::get('FGMOD_DATEADD');
        $this->context->smarty->assign('id_actu', $id_actu);
        $this->context->smarty->assign('titre', $titre);
        $this->context->smarty->assign('actualites', $actualite);
        $this->context->smarty->assign('date_add', $date_add);
    }

    //création du lien de configuration
    public function getContent() {
        $this->processConfiguration(); //appel des fonctions précédentes
        $this->assignConfiguration();//pour un affichage coté back
        return $this->display(__FILE__, 'getContent.tpl');
    }

    //récupération des données dans la BdD
    public function assignHookHome() {
        $this->context->controller->addCSS($this->_path.'views/css/fgmodactu.css', 'all');
        $titre = Configuration::get('FGMOD_TITRE');
        $actualite = Configuration::get('FGMOD_ACTUALITE');
        $id_actu = Tools::getValue('id_actu');//récupération de l'id de l'actu
        $date_add = Tools::getValue('date_add');
        //requete SQL pour extraire les actus de la BdD
        $affiche_actus = Db::getInstance()->executeS('
        SELECT * FROM `' . _DB_PREFIX_ . 'modactua`
        WHERE `id_actu` = ' . (int) $id_actu);
        
        //assignation des variables à Smarty pour l'affichage coté front
        $this->context->smarty->assign('titre', $titre);
        $this->context->smarty->assign('actualites', $actualite);
        $this->context->smarty->assign('affiche_actus', $affiche_actus);
        $this->context->smarty->assign('date_add', $date_add);
    }

    //Permet l'affichage des données sur le hook choisi plus haut, ici page accueil(Home)
    public function hookDisplayHome($params)
    {
        $this->assignHookHome();//appel de la fonction de récupération
        //affichage sur le template
        return $this->display(__FILE__, 'displayHookHome.tpl');
    }
}
