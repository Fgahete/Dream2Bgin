<?php

class fgapiyoutube extends Module {

    //constructeur, définition du module
    public function __construct() {
        //general and technical infos
        $this->name = 'fgapiyoutube';
        $this->version = '0.0.1';
        $this->author = 'Catchu';
        $this->tab = 'front_office_features';

        $this->displayName = $this->l('API Youtube');
        $this->description = $this->l('Vous pouvez ajouter une video Youtube sur votre page Accueil');

        $this->bootstrap = true;

        parent::__construct();
    }

    //installation du module
    public function install() {
        if (!parent::install()) {
            return false;
        }

        if (!$this->registerHook('displayFooterBefore')) {
            //!$this->registerHook('displayHeader') if css
            //displayOverridetemplate
            return false;
        }
        Configuration::updateValue('MODULEYOUTUBE_TITLE', '');
        Configuration::updateValue('MODULEYOUTUBE_URL', '');
        /*Configuration::updateValue('MODULEYOUTUBE_HEIGHT', '');
        Configuration::updateValue('MODULEYOUTUBE_WIDTH', '');*/


        return true;
    }

    //désinstallation du module
    public function uninstall() {
        if (!parent::uninstall()) {
            return false;
        }
        Configuration::deleteByName('MODULEYOUTUBE_TITLE', '');
        Configuration::deleteByName('MODULEYOUTUBE_URL', '');
       /* Configuration::deleteByName('MODULEYOUTUBE_HEIGHT', '');
        Configuration::deleteByName('MODULEYOUTUBE_WIDTH', '');*/

        return true;
    }
    
    
      //Définition de la page de configuration du module
    public function getContent() {
        $html = '';
        $html .= $this->processConfiguration();
        $html .= $this->renderForm();
        return $html;
    }

    //Méthode pour récupérer les valeurs enregistrées afin de remplir le formulaire
    public function getConfigFieldsValues() {
        return array(
            'MODULEYOUTUBE_TITLE' => Tools::getValue('MODULEYOUTUBE_TITLE', Configuration::get('MODULEYOUTUBE_TITLE')),
            'MODULEYOUTUBE_URL' => Tools::getValue('MODULEYOUTUBE_URL', Configuration::get('MODULEYOUTUBE_URL')),/*
            'MODULEYOUTUBE_HEIGHT' => Tools::getValue('MODULEYOUTUBE_HEIGHT', Configuration::get('MODULEYOUTUBE_HEIGHT')),
            'MODULEYOUTUBE_WIDTH' => Tools::getValue('MODULEYOUTUBE_WIDTH', Configuration::get('MODULEYOUTUBE_WIDTH')),*/
        );
    }

    //formulaire de configuration du module
    // http://doc.prestashop.com/display/PS16/Using+the+HelperForm+class
    public function renderForm() {
		/* $heightOptions = array(
            array(
                'id_heightOptions' => '270', // The value of the 'value' attribute of the <option> tag.
                'heightOptions' => '270 pixels'   //affiché // The value of the text content of the  <option> tag.
            ),
            array(
                'id_heightOptions' => '350',
                'heightOptions' => '350 pixels'
            ),
            array(
                'id_heightOptions' => '500',
                'heightOptions' => '500 pixels'
            )
                );
                 
                 $widthOptions = array(
            array(
                'id_widthOptions' => '480', // The value of the 'value' attribute of the <option> tag.
                'widthOptions' => '480 pixels'   //affiché // The value of the text content of the  <option> tag.
            ),
            array(
                'id_widthOptions' => '550',
                'widthOptions' => '550 pixels'
            ),
            array(
                'id_widthOptions' => '700',
                'widthOptions' => '700 pixels'
            )
                );
*/
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),               
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Title'),
                        'name' => 'MODULEYOUTUBE_TITLE',
                        'desc' => $this->l('Donnez un titre'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Youtube ID'),
                        'name' => 'MODULEYOUTUBE_URL',
                        'desc' => $this->l('Saisir un ID de video Youtube'),
                    ),
                    /*array(
                        'type' => 'select',
                        'label' => $this->l('Choisir une hauteur'),
                        'name' => 'MODULEYOUTUBE_HEIGHT',
                        'desc' => $this->l('Selectionnez taille'),
                        'options' => array(
                            'query' => $heightOptions,
                            'id' => 'id_heightOptions',
                            'name' => 'heightOptions'
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Choisir la largeur'),
                        'name' => 'MODULEYOUTUBE_WIDTH',
                        'desc' => $this->l('Selectionnez taille'),
                        'options' => array(
                            'query' => $widthOptions,
                            'id' => 'id_widthOptions',
                            'name' => 'widthOptions'
                        ),
                    ),*/
                ),
                'submit' => array(
                    'title' => $this->l('Enregistrer'),
                )
            ),
        );
        
//enregistrement des valeurs saisies
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();
        
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_module_youtube_form';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        
    return $helper->generateForm(array($fields_form));
    }
    
    //Enregistrement des valeurs de configuration du module dans la base de données et dans la table ps_configuration
      public function processConfiguration(){
          if (Tools::isSubmit('submit_module_youtube_form')){
              Configuration::updateValue('MODULEYOUTUBE_TITLE', Tools::getValue('MODULEYOUTUBE_TITLE'));
              Configuration::updateValue('MODULEYOUTUBE_URL', Tools::getValue('MODULEYOUTUBE_URL'));
              /*Configuration::updateValue('MODULEYOUTUBE_HEIGHT', Tools::getValue('MODULEYOUTUBE_HEIGHT'));
              Configuration::updateValue('MODULEYOUTUBE_WIDTH', Tools::getValue('MODULEYOUTUBE_WIDTH'));*/

              return $this->displayConfirmation('La configuration a été mise à jour avec succès');
          }
      }
      
      //Front
      //Méthode pour récupérer les valeurs en bdd et ainsi créer la map. 
      //Assignation de ces valeurs au tpl
      public function assignValuesToTemplate(){
          $titre = Configuration::get('MODULEYOUTUBE_TITLE');
          $idyoutube = Configuration::get('MODULEYOUTUBE_URL');
          /*$height = Configuration::get('MODULEYOUTUBE_HEIGHT');
          $width = Configuration::get('MODULEYOUTUBE_WIDTH');*/
          
          $this->context->smarty->assign('titre', $titre);
          $this->context->smarty->assign('idyoutube', $idyoutube);
          /*$this->context->smarty->assign('height', $height);
          $this->context->smarty->assign('width', $width);*/
      }
      
      //HOOKS

          public function hookDisplayFooterBefore($params)
    {
        $this->assignValuesToTemplate();
        return $this->display(__FILE__,'getContent.tpl');
    }
}