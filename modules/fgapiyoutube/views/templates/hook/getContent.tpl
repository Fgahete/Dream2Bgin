<!DOCTYPE html>
<html>
    <body>
        <div id="lecteurYtb" style="text-align:center">
            <h3>{$titre}</h3>
            <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
            <div id="player" class="col-lg-6"></div>
            
            <iframe id="ytplayer" type="text/html" width="200" height="200"
                    src="http://www.youtube.com/embed/{$idyoutube}?enablejsapi=1" frameborder="0"></iframe>
        </div>
    </body>
</html>