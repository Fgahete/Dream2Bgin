<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class fgModStaticMap extends Module {

    public function __construct() {
        /* General and technical informations */
        $this->name = 'fgmodstaticmap';
        $this->version = '0.0.1';
        $this->author = 'Catchu';
        $this->tab = 'front_office_features';
        /* $this->ps_versions_compliancy = array('min'=> '1.6.0', 'max' => '1.7.2'); */

        /* Public informations */
        $this->displayName = $this->l('Adding maps into home page.');
        $this->description = $this->l('With this module, you can add map into home page.');

        /* For admin panel from 1.6 */
        $this->bootstrap = true;

        parent::__construct();
    }

    public function install() {
        if (!parent::install()) {
            return false;
        }
        if (!$this->registerHook('displayFooterBefore') || !$this->registerHook('displayHome')) {
            return false;
        }
        Configuration::updateValue('FGMODSTATICMAP_APIKEY', '');
        Configuration::updateValue('FGMODSTATICMAP_ADDRESS', '');
        Configuration::updateValue('FGMODSTATICMAP_TITLE_MARKER', '');
        Configuration::updateValue('FGMODSTATICMAP_COLOR_MARKER', '');
        Configuration::updateValue('FGMODSTATICMAP_SIZE', '');
        Configuration::updateValue('FGMODSTATICMAP_ZOOM', '');
        return true;
    }

    public function uninstall() {
        if (!parent::uninstall()) {
            return false;
        }
        Configuration::deleteByName('FGMODSTATICMAP_APIKEY', '');
        Configuration::deleteByName('FGMODSTATICMAP_ADDRESS', '');
        Configuration::deleteByName('FGMODSTATICMAP_TITLE_MARKER', '');
        Configuration::deleteByName('FGMODSTATICMAP_COLOR_MARKER', '');
        Configuration::deleteByName('FGMODSTATICMAP_SIZE', '');
        Configuration::deleteByName('FGMODSTATICMAP_ZOOM', '');
        return true;
    }

    public function getContent() {
        $html = '';
        $html .= $this->processConfiguration();
        $html .= $this->displayForm();
        return $html;
    }

    public function getConfigFieldsValues() {
        return array(
            'FGMODSTATICMAP_APIKEY' => Tools::getValue('FGMODSTATICMAP_APIKEY', Configuration::get('FGMODSTATICMAP_APIKEY')),
            'FGMODSTATICMAP_ADDRESS' => Tools::getValue('FGMODSTATICMAP_ADDRESS', Configuration::get('FGMODSTATICMAP_ADDRESS')),
            'FGMODSTATICMAP_TITLE_MARKER' => Tools::getValue('FGMODSTATICMAP_TITLE_MARKER', Configuration::get('FGMODSTATICMAP_TITLE_MARKER')),
            'FGMODSTATICMAP_COLOR_MARKER' => Tools::getValue('FGMODSTATICMAP_COLOR_MARKER', Configuration::get('FGMODSTATICMAP_COLOR_MARKER')),
            'FGMODSTATICMAP_SIZE' => Tools::getValue('FGMODSTATICMAP_SIZE', Configuration::get('FGMODSTATICMAP_SIZE')),
            'FGMODSTATICMAP_ZOOM' => Tools::getValue('FGMODSTATICMAP_ZOOM', Configuration::get('FGMODSTATICMAP_ZOOM')),
        );
    }

    public function displayForm() {
        /*$mapSize = array(
            array(
                'id_mapsize' => '600x400',
                'name' => '600x400',
            ),
            array(
                'id_mapsize' => '500x300',
                'name' => '500x300',
            )
        );*/

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Api key'),
                        'name' => 'FGMODSTATICMAP_APIKEY',
                        'desc' => $this->l('Storing API key given by Google Maps.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Address'),
                        'name' => 'FGMODSTATICMAP_ADDRESS',
                        'desc' => $this->l('Write address to display with marker on map.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Title of marker'),
                        'name' => 'FGMODSTATICMAP_TITLE_MARKER',
                        'desc' => $this->l('Write just one letter for title.'),
                    ),
                    array(
                        'type' => 'color',
                        'label' => $this->l('Color of marker'),
                        'name' => 'FGMODSTATICMAP_COLOR_MARKER',
                        'data-hex' => true,
                        'desc' => $this->l('Choose color of marker.'),
                    ),
                    array(
                        'type' => 'html',
                        'label' => $this->l('Map zoom'),
                        'name' => 'FGMODSTATICMAP_ZOOM',
                        'html_content' => '<input type="number" name="FGMODSTATICMAP_ZOOM">',
                        'desc' => $this->l('Choose the zoom.'),
                    ),
                   /* array(
                        'type' => 'select',
                        'label' => $this->l('Map size'),
                        'name' => 'FGMODSTATICMAP_SIZE',
                        'desc' => $this->l('Choose size of marker.'),
                        'options' => array(
                            'query' => $mapSize,
                            'id' => 'id_mapSize',
                            'name' => 'name'),
                    )*/
                    array(
                        'type' => 'text',
                        'label' => $this->l('Map size'),
                        'name' => 'FGMODSTATICMAP_SIZE',
                        'desc' => $this->l('Choose size of map as ?x?.'),
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int) Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $this->fields_form = array();

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit_fg_static_map_form';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }
    /*http://localhost:8080/prestashop/admin7914gvuvr/index.php?controller=AdminModules&configure=dranermap&tab_module=front_office_features&module_name=dranermap&token=3754bb2f392f622a6e13ab4941dfa8be*/

    public function processConfiguration() {
        if (Tools::isSubmit('submit_fg_static_map_form')) {
            Configuration::updateValue('FGMODSTATICMAP_APIKEY', Tools::getValue('FGMODSTATICMAP_APIKEY'));
            Configuration::updateValue('FGMODSTATICMAP_ADDRESS', Tools::getValue('FGMODSTATICMAP_ADDRESS'));
            Configuration::updateValue('FGMODSTATICMAP_TITLE_MARKER', Tools::getValue('FGMODSTATICMAP_TITLE_MARKER'));
            Configuration::updateValue('FGMODSTATICMAP_COLOR_MARKER', Tools::getValue('FGMODSTATICMAP_COLOR_MARKER'));
            Configuration::updateValue('FGMODSTATICMAP_SIZE', Tools::getValue('FGMODSTATICMAP_SIZE'));
            Configuration::updateValue('FGMODSTATICMAP_ZOOM', Tools::getValue('FGMODSTATICMAP_ZOOM'));
            return $this->displayConfirmation('La configuration a été mise à jour avec succès.');
        }
    }

    public function assignValuesToTemplate() {
        $api_key = Configuration::get('FGMODSTATICMAP_APIKEY');
        $address = Configuration::get('FGMODSTATICMAP_ADDRESS');
        $title = Configuration::get('FGMODSTATICMAP_TITLE_MARKER');
        $color = Configuration::get('FGMODSTATICMAP_COLOR_MARKER');
        $size = Configuration::get('FGMODSTATICMAP_SIZE');
        $zoom = Configuration::get('FGMODSTATICMAP_ZOOM');

        $this->context->smarty->assign('api_key', $api_key);
        $this->context->smarty->assign('address', urlencode($address));
        $this->context->smarty->assign('title', $this->transformTitle($title));
        $this->context->smarty->assign('color', $this->transformColor($color));
        $this->context->smarty->assign('size', $size);
        $this->context->smarty->assign('zoom', $zoom);
    }

    private function transformTitle($title) {
        if (strlen($title) > 1) {
            $title = substr($title, 0, 1);
        }
        return $title;
    }

    private function transformColor($col) {
        $col = substr($col, 1);
        return "0x" . $col;
    }

    /*public function hookDisplayHome($params)
    {
        $this->assignValuesToTemplate();
        return $this->display(__FILE__,'displayFooterBefore.tpl');
    }*/
    
    public function hookDisplayFooterBefore($params)
    {
        $this->assignValuesToTemplate();
        return $this->display(__FILE__,'getContent.tpl');
    }
}

?>
