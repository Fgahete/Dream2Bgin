<div id="staticMap" style="text-align: center;
                 width: fit-content;
                 display: inline-block;" class="col-lg-6">
    <img src="https://maps.googleapis.com/maps/api/staticmap?center={$address}$zoom{$zoom}&size={$size}&maptype=roadmap&markers=color:{$color}%7Clabel:{$title}%7C{$address}&key={$api_key}"/>
</div>
