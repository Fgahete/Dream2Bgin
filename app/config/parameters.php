<?php return array (
  'parameters' => 
  array (
    'database_host' => '127.0.0.1',
    'database_port' => '',
    'database_name' => 'prestashop2',
    'database_user' => 'root',
    'database_password' => '',
    'database_prefix' => 'ps_',
    'database_engine' => 'InnoDB',
    'mailer_transport' => 'smtp',
    'mailer_host' => '127.0.0.1',
    'mailer_user' => NULL,
    'mailer_password' => NULL,
    'secret' => '0SC9a12Zug5ArJEVKowhVV8IY9GqnL8DXhhQZ9InPpk2rh8f7H8WVvHX',
    'ps_caching' => 'CacheMemcache',
    'ps_cache_enable' => false,
    'ps_creation_date' => '2018-04-19',
    'locale' => 'fr-FR',
    'cookie_key' => '3rbBMO9eya9x7RtvIpjb8aIC4WunyjmcDfdMbIVD8meR8NRrvKFRB68N',
    'cookie_iv' => 'MN3eytTB',
    'new_cookie_key' => 'def000001789ffc0151ca29a297d3a866f0ea9b19585260712d079be4684970864b03113b4386e5a19165fc96679af8315417017f964a0365e34fe9fa777a1280a36e615',
  ),
);
