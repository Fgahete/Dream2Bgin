<?php /* Smarty version Smarty-3.1.19, created on 2018-04-20 09:31:08
         compiled from "C:\xampp\htdocs\presproj3\modules\welcome\views\templates\lost.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20025ad9973cd16ad8-42262923%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '34fb650a6ccbc0ed839b99f506cd663285cc80e7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\presproj3\\modules\\welcome\\views\\templates\\lost.tpl',
      1 => 1524136744,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20025ad9973cd16ad8-42262923',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ad9973cd1d267_37523306',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad9973cd1d267_37523306')) {function content_5ad9973cd1d267_37523306($_smarty_tpl) {?>

<div class="onboarding onboarding-popup bootstrap">
  <div class="content">
    <p><?php echo smartyTranslate(array('s'=>'Hey! Are you lost?','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</p>
    <p><?php echo smartyTranslate(array('s'=>'To continue, click here:','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</p>
    <div class="text-center">
      <button class="btn btn-primary onboarding-button-goto-current"><?php echo smartyTranslate(array('s'=>'Continue','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</button>
    </div>
    <p><?php echo smartyTranslate(array('s'=>'If you want to stop the tutorial for good, click here:','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</p>
    <div class="text-center">
      <button class="btn btn-alert onboarding-button-stop"><?php echo smartyTranslate(array('s'=>'Quit the Welcome tutorial','d'=>'Modules.Welcome.Admin'),$_smarty_tpl);?>
</button>
    </div>
  </div>
</div>
<?php }} ?>
