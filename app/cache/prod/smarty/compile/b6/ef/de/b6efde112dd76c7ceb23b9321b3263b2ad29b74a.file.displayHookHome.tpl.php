<?php /* Smarty version Smarty-3.1.19, created on 2018-04-20 08:53:45
         compiled from "modules\fgmodactu\views\templates\hook\displayHookHome.tpl" */ ?>
<?php /*%%SmartyHeaderCode:300255ad98e792afdc6-58857634%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b6efde112dd76c7ceb23b9321b3263b2ad29b74a' => 
    array (
      0 => 'modules\\fgmodactu\\views\\templates\\hook\\displayHookHome.tpl',
      1 => 1524153268,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '300255ad98e792afdc6-58857634',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'affiche_actus' => 0,
    'uneactu' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5ad98e792e5bf7_26634871',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5ad98e792e5bf7_26634871')) {function content_5ad98e792e5bf7_26634871($_smarty_tpl) {?><div class="rte">
    <h1>Nos actus</h1>
    <?php  $_smarty_tpl->tpl_vars['uneactu'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['uneactu']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['affiche_actus']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['uneactu']->key => $_smarty_tpl->tpl_vars['uneactu']->value) {
$_smarty_tpl->tpl_vars['uneactu']->_loop = true;
?>
        <h2><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uneactu']->value['titre'], ENT_QUOTES, 'UTF-8');?>
</h2>
        <p><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uneactu']->value['actualite'], ENT_QUOTES, 'UTF-8');?>
 </p>
        <p class="pfoot">Actualité n°<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uneactu']->value['id_modactua'], ENT_QUOTES, 'UTF-8');?>
 / publiée le : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['uneactu']->value['date_add'], ENT_QUOTES, 'UTF-8');?>
</p>
    <?php } ?>
</div><?php }} ?>
